const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/e_commerce").then(()=>console.log("connection Succesfull....  ")).catch((err)=>console.log(err))
const orderSchema = require("./schema")

const insertManyData = async ()=>{
    try{
        const order2 = new orderSchema
    ({
        users_id:"A00012",
        total_items:2,
        products:["smart phone"],
        billing_address:"Hassan",
        shipping_address:"Banglore",
        transaction_status:"Pending",
        payment_mode:"Card",
        payment_status:"Success",
        order_status:"pending"
        })
        const order3 = new orderSchema
    ({
        users_id:"A08976",
        total_items:4,
        products:["Headphone"],
        billing_address:"Bangalore",
        shipping_address:"Hassan",
        transaction_status:"Success",
        payment_mode:"Cod",
        payment_status:"Success",
        order_status:"Dispatched"
        })
        const order4 = new orderSchema
    ({
        users_id:"A96354",
        total_items:1,
        products:["MI Mobile","Backcover"],
        billing_address:"Mysore",
        shipping_address:"Mangalore",
        transaction_status:"Success",
        payment_mode:"Upi",
        payment_status:"Success",
        order_status:"Dispatched"
        })
        const result = await orderSchema
    .insertMany([order2,order3,order4])
        console.log(result)
    }
    catch(err){
        console.log(err)
    } 
   
}
insertManyData()

const deleteTag = async() =>{
    try{
        console.log(orderSchema)
        const result = await orderSchema.deleteOne({users_id:"A00012"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    
}
deleteTag()

const readData = async() =>{
    try{
        const result = await orderSchema.find({shipping_address:"Hassan"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
readData()

const updateOrder = async()=>{
    try{
        const result = await orderSchema.updateOne({users_id:"A96354"},{$set:{payment_status:"Waiting"}},{useFindAndModify:false})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    finally{
        mongoose.connection.close()
    }
}
updateOrder()
