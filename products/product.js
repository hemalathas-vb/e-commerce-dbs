const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/e_commerce").then(()=>console.log("connection Succesfull....  ")).catch((err)=>console.log(err))
const productSchema = require("./schema")

const insertManyData = async ()=>{
    try{
        const product1 = new productSchema({
            name:"Daily Essentials",
            thumbnail:" They are required",
            product_gallery:["https://image1","https://image2"],
            description:".",
            base_price:2200,
            sell_price:1800,
            category_name:"needs",
            tags:["Ux","DesignPage" ,"DesignGraphic" ,"DesignDesign" ,"Elements"],
            additional_information:"Product Detail Page"
        })
        const product2 = new productSchema({
            name:"Women_sec",
            thumbnail:"Women Running Shoes",
            product_gallery:["https://image1","https://image2"],
            description:"Get inspired and keep up with the latest web & mobile app UI design trends",
            base_price:5000,
            sell_price:3500,
            category_name:"Shoes",
            tags:['Women',"Shoes","Running"],
            additional_information:"Join Screenlane for free shipping on every order"
        })
        const product3 = new productSchema({
            name:"Furniture",
            thumbnail:"Kyara upholstered standard bed by zipcode design",
            product_gallery:["https://image1","https://image2"],
            description:" Perfectly balanced wood accets with fabric.",
            base_price:30000,
            sell_price:27000,
            category_name:"Sofa",
            tags:['Sofa',"furniture","living-room"],
            additional_information:" fast delivery"
        })
        const result = await productSchema.insertMany([product1,product2,product3])
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
   
}
insertManyData()

const deleteProductDetails = async() =>{
    try{
        console.log(productSchema)
        const result = await productSchema.deleteOne({ name:"Women_sec"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    
}
deleteProductDetails()

const readByQueryData = async() =>{
    try{
        const result = await productSchema.find({name:"Daily Essentials"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
readByQueryData()

const updateProudct = async()=>{
    try{
        const result = await productSchema.updateOne({name:"Furniture"},{$set:{category_name:"Wood work"}},{useFindAndModify:false})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    finally{
        mongoose.connection.close()
    }
}
updateProudct()