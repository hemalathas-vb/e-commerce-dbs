const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/e_commerce").then(()=>console.log("connection Succesfull....  ")).catch((err)=>console.log(err))
const tagSchema = require("./schema")

const insertManyData = async ()=>{
    try{
        const tag1 = new tagSchema({
            name:"amazon electronics",
            slug:"amazon/electronics"
        })
        const tag2 = new tagSchema({
            name:"Dell laptop",
            slug:"dell/laptop"
        })
        const tag3 = new tagSchema({
            name:"Sony headphone",
            slug:"sony/headphone"
        })
        const result = await tagSchema.insertMany([tag1,tag2,tag3])
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    
}
insertManyData()

const deleteTag = async() =>{
    try{
        console.log(tagSchema)
        const result = await tagSchema.deleteOne({name:"amazon electronics"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    
}
deleteTag()

const readByQueryData = async() =>{
    try{
        const result = await tagSchema.find({name:"Dell laptop"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
readByQueryData()

const updateTagDetails = async()=>{
    try{
        const result = await tagSchema.updateOne({name:"Sony headphone"},{$set:{name:"Sony"}},{useFindAndModify:false})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    finally{
        mongoose.connection.close()
    }
}
updateTagDetails()
