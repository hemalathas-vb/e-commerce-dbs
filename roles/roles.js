const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/e_commerce").then(()=>console.log("connection Succesfull....  ")).catch((err)=>console.log(err))
const roleSchema = require("./schema")

const insertManyData = async ()=>{
    try{
        const role2 = new roleSchema
    ({
            name:"store admin",
            slug:"admin/store"
        })
        const role3 = new roleSchema
    ({
            name:"CustomerC",
            slug:"customer/cart"
        })
        const role4 = new roleSchema
    ({
            name:"Salesman lead",
            slug:"salesman/lead"
        })
        const result = await roleSchema
    .insertMany([role2,role3,role4])
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
   
}
insertManyData()

const deleteUser = async() =>{
    try{
        console.log(roleSchema)
        const result = await roleSchema.deleteOne({name:"Salesman lead"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
deleteUser()

const readData = async() =>{
    try{
        const result = await roleSchema.find({name:"CustomerC"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
readData()

const updateUser = async()=>{
    try{
        const result = await roleSchema.updateOne({name:"store admin"},{$set:{name:"cartstore admin",slug:"admin/store/cart"}},{useFindAndModify:false})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    finally{
        mongoose.connection.close()
    }
}
updateUser()