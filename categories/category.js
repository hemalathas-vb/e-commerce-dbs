const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/e_commerce").then(()=>console.log("connection Succesfull....  ")).catch((err)=>console.log(err))
const categorySchema = require("./schema")

const insertManyData = async ()=>{
    try{
        const category1 = new categorySchema({
            name:"Realme phone",
            slug:"Realme-phone",
            image:"https://Realme-phone",
            description:"realme is an emerging mobile phone brand which is committed to offering mobile phones with powerful performance, stylish design and sincere services."

        })
        const category2 = new categorySchema({
            name:"MI backcover",
            slug:"MI-coverback",
            image:"https://MI-coverback",
            description:"Xiaomi “Just for fans” slogan is the key to understanding the company."
        })
        const category3 = new categorySchema({
            name:"Sony headphone",
            slug:"Sony-headphone",
            image:"https://Sony-headphone",
            description:"Be Moved; Make Believe; It's Sony"
        })
        const result = await categorySchema.insertMany([category1,category2,category3])
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
   
}
insertManyData()

const deleteCategory = async() =>{
    try{
        console.log(categorySchema)
        const result = await categorySchema.deleteOne({name:"Sony headphone"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    
}
deleteCategory()

const readByQuery = async() =>{
    try{
        const result = await categorySchema.find({name:"Realme phone"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
readByQuery()

const updateCategory = async()=>{
    try{
        const result = await categorySchema.updateOne({name:"MI backcover"},{$set:{name:" MI Smart Phone"}},{useFindAndModify:false})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    finally{
        mongoose.connection.close()
    }
}
updateCategory()