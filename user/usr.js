const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/e_commerce").then(()=>console.log("connection Succesfull....  ")).catch((err)=>console.log(err))
const userSchema = require("./schema")

const insertData = async ()=>{
    try{
        const user1 = new userSchema({
            first_name:"Hema",
            last_name:"Latha",
            email:"hema@gmailcom",
            profile_image:"https://hema",
            role:"Admin"
        })
        const user2 = new userSchema({
            first_name:"Abhi",
            last_name:"Ram",
            email:"abhi@gmail.com",
            profile_image:"https://abhi",
            role:"Customer"
        })
        const user3 = new userSchema({
            first_name:"Kavya",
            last_name:"Lokesh",
            email:"kavya@gmail.com",
            profile_image:"https://kavya",
            role:"Customer"
        })
        const result = await userSchema.insertMany([user1,user2,user3])
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
insertData()
// mongoose.connection.close()


const deleteUser = async() =>{
    try{
        console.log(userSchema)
        const result = await userSchema.deleteOne({first_name:"Kavya"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
deleteUser()

const readData = async() =>{
    try{
        const result = await userSchema.find({first_name:"Hema"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
readData()

const updateUser = async()=>{
    try{
        const result = await userSchema.updateOne({first_name:"Abhi"},{$set:{last_name:"Raman"}},{useFindAndModify:false})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    finally{
        mongoose.connection.close()
    }
}
updateUser()
