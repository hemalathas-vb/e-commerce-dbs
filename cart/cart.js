const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/e_commerce").then(()=>console.log("connection Succesfull....  ")).catch((err)=>console.log(err))
const cartSchema = require("./schema")

const insertData = async ()=>{
    try{
        const user1 = new cartSchema({
            product:"Mouse",
            user_id:"A2345",
            product_quantity:3,
            base_price:1000,
            sell_price:800,
            total_price:2400
        })
        const user2 = new cartSchema({
            product:"Keyboard",
            user_id:"C456",
            product_quantity:2,
            base_price:5000,
            sell_price:4500,
            total_price:9000
        })
        const user3 = new cartSchema({
            product:"Bottle",
            user_id:"A1254",
            product_quantity:10,
            base_price:95,
            sell_price:50,
            total_price:500
        })
        const result = await cartSchema.insertMany([user1,user2,user3])
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
   
}
insertData()

const deleteCart = async() =>{
    try{
        console.log(cartSchema)
        const result = await cartSchema.deleteOne({product:"Mouse"})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
deleteCart()

const readcart= async() =>{
    try{
        const result = await cartSchema.find({total_price:500})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
}
readcart()

const updatecart = async()=>{
    try{
        const result = await cartSchema.updateOne({product:"Keyboard"},{$set:{total_price:5001}},{useFindAndModify:false})
        console.log(result)
    }
    catch(err){
        console.log(err)
    }
    
}
updatecart()